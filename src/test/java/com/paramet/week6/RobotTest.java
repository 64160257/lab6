package com.paramet.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }
    
    @Test
    public void shouldCreateRobotsuccess1(){
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }
    
    @Test
    public void shouldCreateRobotsuccess2(){
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }
    
    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals (1, robot.getY());
    }

    @Test
    public void shouldupSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals (true, robot.up());
        assertEquals (0, robot.getY());
    } 

    @Test
    public void shouldRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals (false, result);
        assertEquals (Robot.Y_MIN, robot.getY());
    }
    @Test
    public void shouldRobotUpNFailed1 () {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals (false, result);
        assertEquals (0, robot.getY());
    }

    @Test
    public void shouldRobotUpSuccess () {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals (true, result);
        assertEquals (10, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess () {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals (true, result);
        assertEquals (6, robot.getY());
    }
    @Test
    public void shouldRobotUpNSuccess2 () {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals (true, result);
        assertEquals (0, robot.getY());
    }

}
