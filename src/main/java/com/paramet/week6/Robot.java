package com.paramet.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;  
    private int y;
    public static final int MIN_X = 0;  
    public static final int MIN_Y = 0;   
    public static final int MAX_X = 0;   
    public static final int MAX_Y = 0;
    public static final Object Y_MIN = null;
    public static final int Y_MAX = 0;
    public static final int X_MIN = 0;
    public static final int X_MAX = 0;    
    
    public Robot(String name, char symbol, int x, Object yMin){
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = (int) yMin;

    }

    public Robot(String name, char symbol){
        this(name, symbol, 0, 0);
    }
    public boolean up() {
        if(y== MIN_Y) return false;
        y = y - 1;
        return false;
    }

    public boolean up(int step){
        for(int i=0; i<step; i++){
            if(!up()) {
                return false;
            }
        }
        return true;

    }
    
    public boolean down() {
        if(y == MAX_Y) return false;
        y = y + 1;
        return false;
    }
    public boolean left() {
        x = x - 1;
        return false;
    }
    public boolean right() {
        x = x + 1;
        return false;
    }
    public void print() {
        System.out.println("Robot: " + name + " X:" + x + " Y:" + y);
    }
    public String getName() {
        return name;    
    }
    public char getSymbol() {
        return symbol;       
    }
    public int getX() {
        return symbol;
    }
    public int getY() {
        return symbol;
    }
}
