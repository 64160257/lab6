package com.paramet.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank paramet = new BookBank("Paramet", 50.0);
        paramet.print();   
        
        BookBank mink = new BookBank("Mink", 1000000.0);
        mink.print();  
        mink.withdraw(300000.0);
        mink.print();   

        paramet.deposit(300000.0);
        paramet.print(); 

        BookBank st = new BookBank("St", 2600000.0);
        st.print();   
        st.withdraw(400000.0);
        st.print();  

    }
}
