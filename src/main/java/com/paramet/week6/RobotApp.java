package com.paramet.week6;

public class RobotApp {
    public static void main(String[] args) {
            Robot body = new Robot("Body", 'B', 1, 0);
            Robot petter = new Robot("Petter", 'P', 10, 10);
            body.print();
            body.right();
            body.print();
            petter.print();
            
            for(int i=(int) Robot.Y_MIN; i<=Robot.Y_MAX; i++) {
                for(int x=Robot.X_MIN; x<=Robot.X_MAX; x++){
                    System.out.print("-");
                }
                System.out.println();
        }
    }
    
}
